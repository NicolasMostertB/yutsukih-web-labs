import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { PlaygroundComponent } from './playground/playground.component';



const routes: Routes = [
  { path: "home", component: HomeComponent },
  {
    path: "snake",
    loadChildren: () => import('./snake/snake-game/snake-game.module').then(m => m.SnakeGameModule)
  },
  {
    path: "magic-of-nine",
    loadChildren: () => import('./magic-of-nine/magic-of-nine.module').then(m => m.MagicOfNineModule)
  },
  {
    path: "playground",
    component: PlaygroundComponent
  },
  { path: '', redirectTo:'/home', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
