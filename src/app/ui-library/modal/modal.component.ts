import { Component, ElementRef, Input, OnChanges, OnInit, SimpleChanges, ViewChild } from '@angular/core';

@Component({
  selector: 'app-modal',
  templateUrl: 'modal.component.html',
  styleUrls: ['./modal.component.scss']
})

export class ModalComponent implements OnInit {
  @Input() isOpened = false;

  @ViewChild('dialog', { static: true }) dialog!: ElementRef<HTMLDialogElement>;

  constructor() { }

  ngOnInit() { }

  public showDialog(): void {
    console.log(this.dialog);
    this.dialog.nativeElement.showModal();
  }

  public onCloseBtnClicked() {
    this.dialog.nativeElement.close();
  }
}
