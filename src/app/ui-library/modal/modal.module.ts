import { NgModule } from '@angular/core';
import { ModalComponent } from './modal.component';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
  imports: [MatIconModule],
  exports: [ModalComponent],
  declarations: [ModalComponent],
  providers: [],
})
export class ModalModule { }
