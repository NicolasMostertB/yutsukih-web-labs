import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  @Output() public snavToggle = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  public onToggleSnav = () => {
    this.snavToggle.emit();
  }
}
