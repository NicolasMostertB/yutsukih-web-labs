import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { MatSlideToggle, MatSlideToggleChange } from '@angular/material/slide-toggle';
import { CookieService} from 'ngx-cookie-service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {

  @Output() public snavClosed = new EventEmitter();
  @Output() public themeToggled = new EventEmitter();

  @ViewChild('toggler') toggler: MatSlideToggle;

  constructor(private cookieService: CookieService) { }

  ngOnInit() {


  }

  ngAfterViewInit() {
    const darkThemeOn = this.cookieService.get("darkThemeOn");
    if(darkThemeOn !== undefined){
      if(darkThemeOn === "true"){
        this.toggler.toggle();
        this.toggler.change.emit(new MatSlideToggleChange(this.toggler, true));
      }
    }
  }

  public onSnavClosed = (e: Event) => {
    this.snavClosed.emit();
  }

  public onThemeToggled = (e) => {
    const darkThemeOn = e.checked ? "true": "false";
    this.cookieService.set("darkThemeOn", darkThemeOn, {expires: 1, secure: true, domain: "falcomboi.tk", path: "/"});
    this.themeToggled.emit(e);
  }
}
