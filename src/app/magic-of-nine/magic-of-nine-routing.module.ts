import { RouterModule, Routes } from "@angular/router";
import { MagicOfNineComponent } from "./magic-of-nine.component";
import { NgModule } from '@angular/core'

const routes: Routes = [
  {
    path: '',
    component: MagicOfNineComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MagicOfNineRoutingModule { }
