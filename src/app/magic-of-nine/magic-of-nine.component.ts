import { ChangeDetectionStrategy, Component, OnInit, inject } from '@angular/core';
import { PlayingCard } from './classes/playing-card';
import { Observable, map, first } from 'rxjs';
import { CardsService } from './services/cards.service';

@Component({
  selector: 'magic-of-nine',
  templateUrl: 'magic-of-nine.component.html',
  styleUrls: ['magic-of-nine.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class MagicOfNineComponent implements OnInit {
  private cardsService = inject(CardsService);

  public cardsInHand: Observable<PlayingCard[]>;
  public deck: Observable<PlayingCard[]>;

  public canDrawCard: Observable<boolean>;

  constructor() { }

  ngOnInit() {
    this.deck = this.cardsService.$deck;
    this.cardsInHand = this.cardsService.$hand;

    this.cardsService.initDeck();
    this.cardsService.shuffleDeck();
    this.canDrawCard = this.cardsInHand.pipe(map(cards => cards.length < 7));
  }

  public onDrawCardBtnClick() {
    this.drawCard();
  }

  public onShuffleBtnClick() {
    this.cardsService.shuffleDeck();
  }

  public onResetBtnClick() {
    this.cardsService.initDeck();
    this.cardsService.shuffleDeck();
  }

  public onDeckClicked() {
    this.drawCard();
  }

  private drawCard() {
    this.cardsInHand
      .pipe(
        first(),
        map((cards) => {
          if(cards.length < 7) {
            this.cardsService.drawCard();
          }
        })
      ).subscribe();
  }
}
