import { ESuit } from "../enums/ESuit";

export class PlayingCard {
  public value: number;
  public suit: ESuit;
}
