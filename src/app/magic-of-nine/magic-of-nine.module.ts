import { NgModule } from '@angular/core';

import { MagicOfNineComponent } from './magic-of-nine.component';
import { PlayingCardImgPipe } from './components/playing-card/playing-card.pipe';
import { PlayingCardComponent } from './components/playing-card/playing-card.component';
import { MagicOfNineRoutingModule } from './magic-of-nine-routing.module';
import { CardPileComponent } from './components/card-pile/card-pile.component';
import { CommonModule } from '@angular/common';
import { CardsService } from './services/cards.service';
import { MatButtonModule } from '@angular/material/button';
import { IsCardSelectablePipe } from './components/card-pile/selectable-card.pipe';
import { DragDropModule } from '@angular/cdk/drag-drop';

@NgModule({
  imports: [
    CommonModule,
    MagicOfNineRoutingModule,
    MatButtonModule,
    DragDropModule
  ],
  exports: [MagicOfNineComponent],
  declarations: [
    MagicOfNineComponent,
    PlayingCardComponent,
    CardPileComponent,
    PlayingCardImgPipe,
    IsCardSelectablePipe
  ],
  providers: [CardsService],
})
export class MagicOfNineModule { }
