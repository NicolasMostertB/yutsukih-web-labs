import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'isCardSelectable'
})

export class IsCardSelectablePipe implements PipeTransform {
  transform(isSelectable: boolean, ...args: any[]): string {
    if(isSelectable) {
      return "card selectable";
    } else {
      return "card";
    }
  }
}
