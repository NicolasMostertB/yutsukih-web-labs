import { ChangeDetectionStrategy, Component, HostBinding, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { PlayingCard } from '../../classes/playing-card';
import { CdkDragEnd } from '@angular/cdk/drag-drop';

@Component({
  selector: 'card-pile',
  templateUrl: 'card-pile.component.html',
  styleUrls: ['card-pile.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class CardPileComponent implements OnInit {
  /**
   * The list of cards present in the pile
   */
  @Input() cards: Observable<PlayingCard[]>;

  /**
   * Sets wether or not the face of the card is visible
   */
  @Input() facedown: boolean = false;

  /**
  * Sets number of card per row. 52 is the default.
  */
  @Input() cardsPerRow: number = 52;

  /**
   * Sets the space between each cards. Takes a value in a CSS unit (1fr, 30px, 5rem, etc).
   */
  @Input() overlappingWidth: string = "1px";

  /** Sets wether or not an animation should be shown when hover mouse over the cards of the pile */
  @Input() isSelectable: boolean;

  @HostBinding('style.grid-template-columns') columns;

  constructor() { }

  ngOnInit() {
    this.columns = `repeat(${this.cardsPerRow}, ${this.overlappingWidth})`;
  }

  onDragEnded(event: CdkDragEnd) {
    event.source.reset();
  }
}
