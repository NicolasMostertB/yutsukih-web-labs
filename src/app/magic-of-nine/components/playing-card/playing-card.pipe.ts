import { Pipe, PipeTransform } from '@angular/core';
import { PlayingCard } from '../../classes/playing-card';
import { ESuit } from '../../enums/ESuit';

@Pipe({
  name: 'playingCardImg'
})
export class PlayingCardImgPipe implements PipeTransform {
  numberCardValues = [2, 3, 4, 5, 6, 7, 8, 9, 10];
  faceCardValues = [
    { value: 1, faceName: "ace" },
    { value: 11, faceName: "jack" },
    { value: 12, faceName: "queen" },
    { value: 13, faceName: "king" }
  ];

  transform(card: PlayingCard, facedown: boolean): any {
    if(!this.numberCardValues.includes(card.value) && !this.faceCardValues.some(x => x.value === card.value)){
      throw new Error("Invalid card value !");
    }

    if(facedown) {
      return '../../../../assets/images/playing-cards/black_joker.png';
    } else {
      const pathToImg = '../../../../assets/images/playing-cards/';
      const fileName = `${this.getValuePartOfFileName(card)}${this.getSuitPartOfFileName(card)}.png`;
      return `${pathToImg}${fileName}`;
    }
  }

  private getValuePartOfFileName(card: PlayingCard): string {
    let fileNamePart = "";

    if (this.numberCardValues.includes(card.value)) {
      fileNamePart = `${card.value}_of_`;
    } else if (this.faceCardValues.some(x => x.value === card.value)) {
      let faceName = this.faceCardValues
        .find(x => x.value === card.value)
        .faceName;
      fileNamePart = `${faceName}_of_`;
    }

    return fileNamePart;
  }

  private getSuitPartOfFileName(card: PlayingCard): string {
    let fileNamePart = "";
    switch (card.suit) {
      case ESuit.Spade:
        fileNamePart = "spades";
        break;
      case ESuit.Club:
        fileNamePart = "clubs";
        break;
      case ESuit.Diamond:
        fileNamePart = "diamonds";
        break;
      case ESuit.Heart:
        fileNamePart = "hearts";
        break;
      default:
        break;
    }

    return fileNamePart;
  }
}
