import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';
import { PlayingCard } from '../../classes/playing-card';

@Component({
  selector: 'playing-card',
  templateUrl: 'playing-card.component.html',
  styleUrls: ['playing-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class PlayingCardComponent implements OnInit {
  @Input() card: PlayingCard;
  @Input() facedown: boolean = false;

  constructor() { }

  ngOnInit() { }
}
