export enum ESuit {
  Spade = 0,
  Diamond = 1,
  Club = 2,
  Heart = 3
}
