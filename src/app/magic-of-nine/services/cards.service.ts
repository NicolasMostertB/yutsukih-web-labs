import { Injectable } from '@angular/core';
import { PlayingCard } from '../classes/playing-card';
import { BehaviorSubject } from 'rxjs';
import { ESuit } from '../enums/ESuit';
import { fisherYatesShuffle, randomSort } from 'src/app/shared/utility/randomizer';

@Injectable()
export class CardsService {
  private deckSubject: BehaviorSubject<PlayingCard[]> = new BehaviorSubject<PlayingCard[]>([]);
  public $deck = this.deckSubject.asObservable();

  private handSubject: BehaviorSubject<PlayingCard[]> = new BehaviorSubject<PlayingCard[]>([]);
  public $hand = this.handSubject.asObservable();

  constructor() { }

  public initDeck() {
    const cards = [];

    for (let i = 1; i <= 13; i++) {
      for (let j = 0; j < 4; j++) {
        cards.push(<PlayingCard>{ suit: j as ESuit, value: i });
      }
    }

    this.deckSubject.next(cards);
    this.handSubject.next([]);
  }

  public drawCard() {
    let deck = this.deckSubject.value;
    let hand = this.handSubject.value;

    hand.push(deck.pop());

    this.deckSubject.next(deck);
    this.handSubject.next(hand);
  }

  public shuffleDeck() {
    const randomDeck = fisherYatesShuffle(this.deckSubject.value.sort(randomSort()));
    this.deckSubject.next(randomDeck);
  }
}
