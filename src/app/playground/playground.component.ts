import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalComponent } from '../ui-library/modal/modal.component';

@Component({
  selector: 'playground',
  templateUrl: 'playground.component.html',
  styleUrls: ['playground.component.scss'],
})

export class PlaygroundComponent implements OnInit {
  @ViewChild('appDialog') myDialog: ModalComponent;

  ngOnInit() { }

  onShowBtnClicked() {
    console.log(this.myDialog);
    this.myDialog.showDialog();
  }
}
