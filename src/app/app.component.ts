import {MediaMatcher} from '@angular/cdk/layout';
import {ChangeDetectorRef, Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'YutsukihWebLabs';
  mobileQuery: MediaQueryList;
  themeClassName: string;

  private _mobileQueryListener: () => void;

  constructor(changeDetectorRef: ChangeDetectorRef, media: MediaMatcher) {
    this.mobileQuery = media.matchMedia('(max-width: 768px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addEventListener("change", this._mobileQueryListener);
    this.themeClassName = '';
  }

  ngOnDestroy(): void {
    this.mobileQuery.addEventListener("change", this._mobileQueryListener);
  }

  toggleTheme(e): void {
    this.themeClassName = e.checked ? 'darkMode' : '';
  }
}
