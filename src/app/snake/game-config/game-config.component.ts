import { Component, OnInit } from '@angular/core';
import { Theme } from '../classes/theme';
import { GameConfigService } from '../services/game-config.service';
import { RenderingService } from '../services/rendering.service';
import { SnakeGameService } from '../services/snake-game.service';

export class ListItem {
  value: number;
  viewValue: string;
}


@Component({
  selector: 'app-game-config',
  templateUrl: './game-config.component.html',
  styleUrls: ['./game-config.component.scss']
})
export class GameConfigComponent implements OnInit {

  themes: Theme[];

  value = 100;
  min = 50;
  max = 200;

  selectedTheme: number;
  disableControls: Boolean;

  constructor(private gameConfig: GameConfigService, private snakeService: SnakeGameService, private renderService: RenderingService) { }

  ngOnInit() {
    this.themes = this.gameConfig.themes;
    this.selectedTheme = 1;
    this.gameConfig.selectedThemeId = this.selectedTheme;

    this.snakeService.gameRunning$.subscribe((value) =>{
      this.disableControls = value;
    })
  }

  onValueChanged(){
    let factor = this.value / 100;
    this.gameConfig.setDelayWithFactor(factor);
  }

  onSelectedThemeChanged(e){
    this.gameConfig.selectedThemeId = e.value;
    this.gameConfig.setDefaultTheme();
    this.renderService.clearBoard(this.gameConfig.boardWidth, this.gameConfig.boardheight);
  }
}
