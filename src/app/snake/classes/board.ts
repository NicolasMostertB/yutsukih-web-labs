import { Snake } from "./snake"
import { EDirection } from "./direction";
import { FoodSpace } from "./foodSpace";
import { RenderingService } from "../services/rendering.service";
import { ScoreService } from "../services/score.service";
import { GameConfigService } from "../services/game-config.service";

export class Board {
  snake: Snake;

  width: number;
  height: number;

  food: FoodSpace;

  constructor(height: number, width: number, private renderService: RenderingService, private scoreService: ScoreService, private gameConfig: GameConfigService) {
    this.snake = new Snake(renderService, this.gameConfig);
    this.width = width;
    this.height = height;
  }

  getFoodSpace() : FoodSpace {
    return this.food;
  }

  moveSnake(direction: EDirection) {
    switch(direction ){
      case EDirection.UP: {
        this.snake.moveSnake(0, this.gameConfig.spaceSize * -1);
        break;
      }
      case EDirection.DOWN: {
        this.snake.moveSnake(0, this.gameConfig.spaceSize);
        break;
      }
      case EDirection.LEFT: {
        this.snake.moveSnake(this.gameConfig.spaceSize * -1, 0);
        break;
      }
      case EDirection.RIGHT: {
        this.snake.moveSnake(this.gameConfig.spaceSize, 0);
        break;
      }
    }
  }

  validateLosingRules() : boolean {
    return !this.snake.isSnakeEatingItself() && this.didSnakeBumpIntoWall();
  }

  didSnakeBumpIntoWall(): boolean {
    let head = this.snake.getSnakeHead();
    return head.x >= 0 && head.x < this.width && head.y >= 0 && head.y < this.height;
  }

  renderCurrentSnake() {
    if(this.snake.isSnakeEatingFood(this.food)) {
      this.snake.growSnake();
      this.generateNewFood();
      this.scoreService.addPointsToScore();
    }

    this.renderService.clearBoard(this.width, this.height);
    this.snake.renderSnakeSpaces();
    this.renderFoodSpace();
  }

  generateNewFood() {
    let newFood: FoodSpace;
    let bodyParts = this.snake.getSnakeBodyParts();

    while(newFood === undefined){
      let xPos = Math.floor(Math.random() * (this.width / this.gameConfig.spaceSize)) * this.gameConfig.spaceSize;
      let yPos = Math.floor(Math.random() * (this.height / this.gameConfig.spaceSize)) * this.gameConfig.spaceSize;

      if(bodyParts.find(item => item.x === xPos && item.y === yPos) === undefined) {
        newFood = new FoodSpace(xPos, yPos);
      }
    }

    this.food = newFood;
  }

  renderFoodSpace() {
    this.renderService.drawSnakePart(this.food)
  }
}
