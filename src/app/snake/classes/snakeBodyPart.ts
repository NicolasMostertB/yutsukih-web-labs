import { ESpaceType } from '../enums/eSpaceType.enum';

export class SnakeSpace {
   nextSnakeSpace: SnakeSpace;
   previousSnakeSpace: SnakeSpace;
   x: number;
   y: number;
   type: ESpaceType;

   constructor(x: number, y: number) {
	  this.x = x;
     this.y = y;
     this.nextSnakeSpace = null;
     this.previousSnakeSpace = null;
     this.type = ESpaceType.snakeBody;
   }

   SetPrevious(item: SnakeSpace) {
	  this.previousSnakeSpace = item;
   }

   SetNext(item: SnakeSpace) {
	  this.nextSnakeSpace = item;
   }
}
