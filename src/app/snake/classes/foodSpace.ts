﻿import { ESpaceType } from "../enums/eSpaceType.enum";

export class FoodSpace {
    x: number;
    y: number;
    type: ESpaceType;

    constructor(x: number, y: number){
        this.x = x;
        this.y = y;
        this.type = ESpaceType.food;
    }
}