import { ɵsetCurrentInjector } from "@angular/core";
import { RenderingService } from "../services/rendering.service";
import { FoodSpace } from "./foodSpace";
import { SnakeSpace } from "./snakeBodyPart"
import { ESpaceType} from '../enums/eSpaceType.enum';
import { GameConfigService } from "../services/game-config.service";

export class Snake {
  bodyParts: SnakeSpace[];
  currentHead: SnakeSpace;
  currentTail: SnakeSpace;

  lastTail: SnakeSpace;

  constructor(private renderService: RenderingService, private gameConfig: GameConfigService) {
    this.bodyParts = [];
    this.CreatInitialSnakeSpaces();
  }

  getSnakeHead() : SnakeSpace {
    return this.currentHead;
  }

  getSnakeBodyParts() : SnakeSpace[] {
    return this.bodyParts;
  }

  CreatInitialSnakeSpaces() {
    const offset = this.gameConfig.spaceSize;

    let head = new SnakeSpace(120, 90);
    let middle = new SnakeSpace(120 + offset, 90);
    let tail = new SnakeSpace(120 + (offset * 2), 90);

    head.SetPrevious(middle);
    head.type = ESpaceType.snakeHead;

    middle.SetNext(head);
    middle.SetPrevious(tail);

    tail.SetNext(middle);

    this.bodyParts.push(head);
    this.bodyParts.push(middle);
    this.bodyParts.push(tail);

    this.currentHead = head;
    this.currentTail = tail;

    this.lastTail = tail;
  }

  moveSnake(offsetX: number, offsetY: number){
    let newHead = new SnakeSpace(this.currentHead.x + offsetX, this.currentHead.y + offsetY);
    newHead.type = ESpaceType.snakeHead;
    this.currentHead.type = ESpaceType.snakeBody;

    if(newHead.y === this.gameConfig.boardheight){ //TODO: Harcoded
      newHead.y = 0;
    }
    else if(newHead.y === this.gameConfig.spaceSize * -1){
      newHead.y = this.gameConfig.boardheight - this.gameConfig.spaceSize;
    }
    else if(newHead.x === this.gameConfig.boardWidth){
      newHead.x = 0;
    }
    else if(newHead.x === this.gameConfig.spaceSize * -1){
      newHead.x = this.gameConfig.boardWidth - this.gameConfig.spaceSize;
    }

    this.bodyParts[0].nextSnakeSpace = newHead;
    newHead.previousSnakeSpace = this.currentHead;

    this.currentHead = newHead;
    this.bodyParts.unshift(newHead);
    this.lastTail = this.bodyParts.pop();
    this.bodyParts[this.bodyParts.length - 1].previousSnakeSpace = null;
    this.currentTail = this.bodyParts[this.bodyParts.length -1];
  }

  isSnakeEatingItself() : boolean {
    let part = this.bodyParts.find(x => x.x === this.currentHead.x && x.y === this.currentHead.y && x.nextSnakeSpace !== null)
    if (part !== undefined){
      return true;
    }

    return false;
  }

  isSnakeEatingFood(food: FoodSpace) {
    let isEatingFood = false;
    if(this.currentHead.x === food.x && this.currentHead.y === food.y) {
      isEatingFood = true;
    }
    return isEatingFood;
  }

  growSnake() {
    let newBodyPart = new SnakeSpace(this.lastTail.x, this.lastTail.y);

    this.currentTail.SetPrevious(newBodyPart);
    newBodyPart.SetNext(this.currentTail);
    this.bodyParts.push(newBodyPart);
    this.currentTail = newBodyPart;
    this.lastTail = newBodyPart;
  }

  renderSnakeSpaces() {
    this.bodyParts.forEach(item => {
      if(item.nextSnakeSpace !== null){
        this.renderService.drawSnakePart(item);
      }
      else {
        this.renderService.drawSnakePart(item);
      }
    });
  }
}
