export class Highscore{
    name: string;
    score: number;
    createdOn: Date;
    id: number;
}