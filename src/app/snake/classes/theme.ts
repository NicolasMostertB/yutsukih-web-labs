export class Theme {
    id: number;
    description: string;

    snakeBodyFillColor: string;
    snakeBodyStrokeColor: string;

    snakeHeadFillColor: string;
    snakeHeadStrokeColor: string;

    foodFillColor: string;
    foodStrokeColor: string;

    boardBackgroundColor: string;
    boardBackgroundStrokeColor: string;
}