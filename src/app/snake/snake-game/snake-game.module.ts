import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SnakeComponent } from '../snake.component';
import { MatButtonModule } from '@angular/material/button';
import { MatListModule } from '@angular/material/list';
import { MatSliderModule } from '@angular/material/slider';
import { GameConfigComponent } from '../game-config/game-config.component';
import { ScoreComponent } from '../score/score.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatCardModule } from '@angular/material/card';
import { MatDialogModule } from '@angular/material/dialog';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { SnakeGameRoutingModule } from './snake-game-routing.module';
import { NewHighscoreDialog } from '../score/new-highscore-dialog.component';

@NgModule({
  declarations: [
    SnakeComponent,
    GameConfigComponent,
    ScoreComponent,
    NewHighscoreDialog
  ],
  imports: [
    SnakeGameRoutingModule,
    CommonModule,
    MatButtonModule,
    MatListModule,
    MatFormFieldModule,
    MatSelectModule,
    MatSliderModule,
    MatCardModule,
    MatDialogModule,
    FormsModule,
    MatInputModule
  ],
  exports: [
    SnakeComponent,
    NewHighscoreDialog
  ],
  providers: [
    NewHighscoreDialog
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]

})
export class SnakeGameModule { }
