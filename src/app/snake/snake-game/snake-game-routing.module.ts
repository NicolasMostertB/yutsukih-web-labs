import { RouterModule, Routes } from "@angular/router";
import { SnakeComponent } from "../snake.component";
import { NgModule } from '@angular/core'

const routes: Routes = [
  {
    path: '',
    component: SnakeComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SnakeGameRoutingModule { }
