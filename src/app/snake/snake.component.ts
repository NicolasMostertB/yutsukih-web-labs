import { MediaMatcher } from '@angular/cdk/layout';
import { Component, OnInit, HostListener, ViewChild, ElementRef, AfterViewInit  } from '@angular/core';
import { SnakeGameService } from './services/snake-game.service';

@Component({
  selector: 'app-snake',
  templateUrl: './snake.component.html',
  styleUrls: ['./snake.component.scss']
})
export class SnakeComponent implements OnInit, AfterViewInit {
  @ViewChild('snakeCanvas') canvas: ElementRef<HTMLCanvasElement>;

  gameRunning: Boolean;
  boardWidth: number;
  boardHeight: number;

  constructor(private snakeGame: SnakeGameService, media: MediaMatcher)
  {
    const matches = media.matchMedia('(max-width: 768px)');

    if(matches.matches){
      this.boardHeight = 300;
      this.boardWidth = 300;
    } else{
      this.boardHeight = 450;
      this.boardWidth = 450;
    }
    this.snakeGame.gameRunning$.subscribe((value) =>{
      this.gameRunning = value;
    })
  }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    this.snakeGame.init(this.canvas.nativeElement);
  }

  cmdStartGame_Clicked(): void {
    document.getElementById("cmdStartGame").blur();
    this.snakeGame.StartTheGame();

  }

  @HostListener('document:keydown', ['$event'])
  keyEvent(event: KeyboardEvent) {
    this.snakeGame.setCurrentDirection(event.code);
  }

  onPanLeft(e){
    this.snakeGame.setCurrentDirection("ArrowLeft");
  }

  onPanRight(e){
    this.snakeGame.setCurrentDirection("ArrowRight");
  }

  onPanUp(e){
    this.snakeGame.setCurrentDirection("ArrowUp");
  }

  onPanDown(e){
    this.snakeGame.setCurrentDirection("ArrowDown");
  }
}
