import { Component, OnInit } from '@angular/core';
import { Highscore } from '../classes/highscore';
import { ScoreService } from '../services/score.service';
import { MatDialog } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { NewHighscoreDialog } from './new-highscore-dialog.component';

@Component({
  selector: 'app-score',
  templateUrl: './score.component.html',
  styleUrls: ['./score.component.scss']
})
export class ScoreComponent implements OnInit {

  currentScore: Observable<number>;
  highscores : Observable<Highscore[]>;

  displayScores;
  name: string;

  constructor(private scoreService: ScoreService, public dialog: MatDialog)
  {
    this.currentScore = scoreService.currentScore$;
    this.highscores = scoreService.highscores$;

    scoreService.newHighscore.subscribe(() =>{
      this.openDialog();
    });
  }

  openDialog(){
    const dialogRef = this.dialog.open(NewHighscoreDialog, {
      data: {score: this.currentScore}
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result != undefined && result !== "")
        this.scoreService.saveNewHighscore(result);
    });
  }

  ngOnInit() {
  }
}
