import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

export interface DialogData {
  name: string;
  score: number;
}

@Component({
  selector: 'new-highscore-dialog',
  templateUrl: 'new-highscore-dialog.html',
})
export class NewHighscoreDialog {
  constructor(public dialogRef: MatDialogRef<NewHighscoreDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) { }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
