import { MediaMatcher } from '@angular/cdk/layout';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Board } from "../classes/board";
import { EDirection } from '../classes/direction';
import { GameConfigService } from './game-config.service';
import { RenderingService } from './rendering.service';
import { ScoreService } from './score.service';

@Injectable({
  providedIn: 'root'
})
export class SnakeGameService {

  board: Board;
  direction: EDirection;
  loopFinished: boolean;

  private gameRunningSubject = new BehaviorSubject<Boolean>(false);
  public gameRunning$ = this.gameRunningSubject.asObservable();

  constructor(private renderService: RenderingService, private config: GameConfigService, private scoreService: ScoreService, media: MediaMatcher) {
    const matches = media.matchMedia('(max-width: 768px)');

    if (matches.matches) {
      this.config.boardWidth = 300;
      this.config.boardheight = 300;
      this.config.spaceSize = 10;
    } else {
      this.config.boardWidth = 450;
      this.config.boardheight = 450;
      this.config.spaceSize = 15;
    }
  }

  init(canvas: HTMLCanvasElement) {
    this.board = new Board(this.config.boardheight, this.config.boardWidth, this.renderService, this.scoreService, this.config);
    this.renderService.setCanvas(canvas);
  }

  StartTheGame(): void {
    this.scoreService.resetScore();
    this.gameRunningSubject.next(true);

    this.board = new Board(this.config.boardheight, this.config.boardWidth, this.renderService, this.scoreService, this.config);
    this.renderService.clearBoard(this.config.boardWidth, this.config.boardheight);
    this.board.generateNewFood();
    this.board.snake.renderSnakeSpaces();
    this.board.renderFoodSpace();

    this.Loop();
  }

  async Loop() {
    let gameOver = false;
    while (!gameOver) {
      this.board.moveSnake(this.direction)

      gameOver = !this.board.validateLosingRules();

      if (!gameOver) {
        this.board.renderCurrentSnake();
      }
      else {
        this.direction = null;
      }
      this.loopFinished = true;
      await this.delay(this.config.delay);
    };
    this.direction = null;
    this.gameRunningSubject.next(false);
    this.scoreService.checkForNewHighScore();
  }

  delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  setCurrentDirection(direction: string) {
    if (this.loopFinished && this.gameRunningSubject.getValue()) {
      this.loopFinished = false;
      switch (direction) {
        case "ArrowLeft": {
          if (this.direction !== EDirection.RIGHT) {
            this.direction = EDirection.LEFT;
          }
          break;
        }
        case "ArrowRight": {
          if (this.direction !== EDirection.LEFT) {
            this.direction = EDirection.RIGHT;
          }
          break;
        }
        case "ArrowDown": {
          if (this.direction !== EDirection.UP) {
            this.direction = EDirection.DOWN;
          }
          break;
        }
        case "ArrowUp": {
          if (this.direction !== EDirection.DOWN) {
            this.direction = EDirection.UP;
          }
          break;
        }
      }
    }
  }
}
