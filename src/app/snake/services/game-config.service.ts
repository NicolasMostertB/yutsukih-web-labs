import { Injectable } from '@angular/core';
import { Theme } from '../classes/theme';

const DEFAULT_DELAY = 80;
const DEFAULT_SIZE = 10;

@Injectable({
  providedIn: 'root'
})
export class GameConfigService {
  themes: Theme[] = this.getHardcodedThemes();

  delay: number;
  spaceSize: number;
  boardWidth: number;
  boardheight: number;

  selectedThemeId: number;
  selectedTheme: Theme;

  constructor()
  {
    this.selectedThemeId = 1;
    this.setDefaultTheme();
    this.setDefaultConfig();
  }

  // TODO: save themes in database, json them into objects and no more hardcoding any values
  setDefaultTheme(){
    let theme = this.themes.find(x => x.id === this.selectedThemeId);
    this.selectedTheme = theme;

  }

  setDefaultConfig(){
    this.delay = DEFAULT_DELAY;
    this.spaceSize = DEFAULT_SIZE;
  }

  setDelayWithFactor(factor){
    this.delay = DEFAULT_DELAY / factor;
  }

  private getHardcodedThemes(): Theme[] {
    return [
      { id: 1, description: "Default", snakeBodyFillColor: "lightblue", snakeBodyStrokeColor: "darkblue", snakeHeadFillColor: "blue",
      snakeHeadStrokeColor: "darkblue", foodFillColor: "red", foodStrokeColor: "darkred", boardBackgroundColor: "white", boardBackgroundStrokeColor: "black"
      },
      { id: 2, description: "Red snake", snakeBodyFillColor: "#ff0b03", snakeBodyStrokeColor: "darkred", snakeHeadFillColor: "#ad0903",
      snakeHeadStrokeColor: "darkred", foodFillColor: "#fff81f", foodStrokeColor: "#ffb303", boardBackgroundColor: "#3b3939", boardBackgroundStrokeColor: "black"
      }
    ];

  }
}
