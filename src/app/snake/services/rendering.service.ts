import { Injectable } from '@angular/core';
import { ESpaceType } from '../enums/eSpaceType.enum';
import { GameConfigService } from './game-config.service';

@Injectable({
  providedIn: 'root'
})
export class RenderingService {
  private canvas: HTMLCanvasElement;
  private canvasContext : CanvasRenderingContext2D;

  constructor(private config: GameConfigService) { }

  setCanvas(canvas: HTMLCanvasElement){
    this.canvas = canvas;//<HTMLCanvasElement> document.getElementById("snakeCanvas")
    this.canvasContext = this.canvas.getContext("2d");
  }

  drawSnakePart(snakePart){
    let fillColor = ''
    let strokeColor = ''

    let theme = this.config.selectedTheme;

    switch(snakePart.type){
      case ESpaceType.snakeHead: {
        fillColor = theme.snakeHeadFillColor;
        strokeColor = theme.snakeHeadStrokeColor;
        break;
      }
      case ESpaceType.snakeBody: {
        fillColor = theme.snakeBodyFillColor;
        strokeColor = theme.snakeBodyStrokeColor;
        break;
      }
      case ESpaceType.food: {
        fillColor = theme.foodFillColor;
        strokeColor = theme.foodStrokeColor;
        break;
      }
    }

    this.canvasContext.fillStyle = fillColor;
    this.canvasContext.strokeStyle = strokeColor;
    this.canvasContext.fillRect(snakePart.x, snakePart.y, this.config.spaceSize, this.config.spaceSize);
    this.canvasContext.strokeRect(snakePart.x, snakePart.y, this.config.spaceSize, this.config.spaceSize);
  }

  clearBoard(width: number, height: number) {
    let theme = this.config.selectedTheme;

    this.canvasContext.fillStyle = theme.boardBackgroundColor;
    this.canvasContext.strokeStyle = theme.boardBackgroundStrokeColor;
    this.canvasContext.fillRect(0, 0, width, height);
    this.canvasContext.strokeRect(0, 0, width, height);
  }


}
