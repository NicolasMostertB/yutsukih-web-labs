import { HttpClient } from '@angular/common/http';
import { EventEmitter, Injectable, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { BehaviorSubject } from 'rxjs';
import { Highscore } from '../classes/highscore';

@Injectable({
  providedIn: 'root'
})
export class ScoreService {
  private highscores: BehaviorSubject<Highscore[]> = new BehaviorSubject([]);
  public highscores$ = this.highscores.asObservable();

  private currentScoreSubject = new BehaviorSubject<number>(0);
  public currentScore$ = this.currentScoreSubject.asObservable();

  name: string;

  @Output() public newHighscore = new EventEmitter;

  constructor(public dialog: MatDialog, private http: HttpClient)
  {
    this.setCurrentHighscores();
    this.highscores = new BehaviorSubject<Highscore[]>([]);
  }

  addPointsToScore(){
    this.currentScoreSubject.next(this.currentScoreSubject.getValue() + 5);
  }

  resetScore(){
    this.currentScoreSubject.next(0);
  }

  public checkForNewHighScore(){
    let forceNewHighscore = this.highscores.getValue().length < 5;
    let smallerScore = this.highscores.value.find(x => x.score < this.currentScoreSubject.getValue());

    if(smallerScore !== undefined || forceNewHighscore){
      this.newHighscore.emit();
    }
  }

  saveNewHighscore(name: string) : void{
    let newHighscore = new Highscore();
    newHighscore.name = name;
    newHighscore.score = this.currentScoreSubject.getValue();

    const list = this.highscores.value;
    list.push(newHighscore);
    list.sort((a, b) => b.score - a.score); // descending

    if(list.length > 5)
      list.pop();

    this.highscores.next(list);

    this.postNewHighscore(newHighscore.score, name);
  }

  postNewHighscore(score: number, name: string): void {
  //   this.http.post<any>(`https://reanshwarzer.tk/highscore?score=${score}&name=${name}`, null)
  //     .subscribe((result) => console.log(result), (error) => console.log(error));
  }

  setCurrentHighscores() : void {
    // this.http.get<Highscore[]>("https://reanshwarzer.tk/highscore")
    //   .subscribe((result) =>{
    //     this.highscores.next(result);
    //   });
  }
}

