import { inject } from "@angular/core"
import { ActivatedRoute } from "@angular/router"
import { Observable, filter, map } from "rxjs"

export const injectIdParam = (): Observable<number> => {
  return inject(ActivatedRoute).queryParams
    .pipe(
      filter(params => params['id']),
      map(params => Number.parseInt(params['id']))
    );
}
